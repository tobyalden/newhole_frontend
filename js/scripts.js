if(!document.fullscreenEnabled) {
    $("#fullscreen").addClass("hidden");
}

// https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);
window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Loading the YouTube API (must be done in global scope)
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: 'vUySIPTxtvQ',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange,
            'onError': onError
        },
        playerVars: {
            controls: 0,
            disablekb: 1,
            modestbranding: 1,
            playsinline: 1
        }
    });
}

var isPlayerReady = false;
function onPlayerReady(event) {
    log('YouTube API loaded.');
    $('#playicon').removeClass('hidden');
    isPlayerReady = true;
}

function onError(event) {
    log("YouTube error (code " + event.data + "). Restarting...");
    findVideo();
}

var staticSfx = new Audio('public/static.mp3');
staticSfx.loop = true;

var started = false;
var firstVideoFound = false;
var censorTimeout;
function onPlayerStateChange(event) {
    if(event.data == YT.PlayerState.BUFFERING && !started) {
        player.pauseVideo();
        start();
    }
    if(event.data == YT.PlayerState.PLAYING && firstVideoFound) {
        $('#player').css('opacity', 1);
        $('#static').addClass('hidden');
        staticSfx.pause();
        $('#censor').removeClass('hidden');
        censorTimeout = setTimeout(hideCensor, 4000);
    }
    else if(event.data == YT.PlayerState.ENDED) {
        skipVideo();
        console.log('video ended... skipping');
    }
}

function start() {
    if(started) {
        return;
    }
    staticSfx.play();
    $('#static').removeClass('hidden');
    $('#overlay').removeClass('hidden');
    $('#playicon').addClass('hidden');
    $('#warning').addClass('hidden');
    started = true;
    findVideo();
}

function findVideo() {
    $.ajax({
        type: "GET",
        url: "https://api.youhole.tv/unwatched",
        success: function(response) {
            firstVideoFound = true;
            player.loadVideoById(response[0].video_id);
        },
        error: handleAjaxError
    });
}

function handleAjaxError(xhr) {
    log('Error! Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ResponseText: ' + xhr.responseText, true);
    findVideo();
}

function log(message, isError = false) {
    if(!isError) {
        return;
    }
    console.log(message);
    //$('#errors').append(message + '<br>');
}

$("#fullscreen").click(function(e) {
    document.body.requestFullscreen();
});

document.onfullscreenchange = function(e) {
    if(e.currentTarget.fullscreen) {
        $("#fullscreen").addClass("hidden");
    }
    else {
        $("#fullscreen").removeClass("hidden");
    }
}

$(document).on('click keypress', function(e) {
    if(e.target.id == "fullscreen" || e.target.href != undefined) {
        return;
    }
    if(!started) {
        start();
    }
    else if($('#static').hasClass("hidden")) {
        skipVideo();
        console.log('clicked... skipping');
    }
});

function skipVideo() {
    if(censorTimeout != undefined) {
        clearTimeout(censorTimeout);
        log("Cleared censorTimeout.");
    }
    staticSfx.play();
    player.pauseVideo();
    $('#static').removeClass('hidden');
    $('#censor').addClass('hidden');
    findVideo();
}

function hideCensor() {
    $('#censor').addClass('hidden');
}
